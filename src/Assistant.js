'use strict';
const React = require('react')
const { useState } = require('react');
const ink = require('ink')
const hotkeys = require('node-hotkeys')
const importJsx = require('import-jsx')
const R = require('ramda')

const useStore = require('./store/useStore')
const taskLib = require('./taskLib');

const TitleBar = importJsx('ink-components/TitleBar')({React, ink, hotkeys})
const LogWindow = importJsx('ink-components/LogWindow')({React, ink})

const {Text, Box, useFocus, useInput} = ink
const Task = ({number, name, onEnter}) => {
  const {isFocused} = useFocus({autoFocus: (number == 1)})

  if (onEnter != undefined) {
    useInput((input, key) => {
      if (key.return == true && isFocused == true) {
        onEnter()
      }
    })
  }

  return (
    <Text
      bold={isFocused}
    >
      {number}. {name}
    </Text>
  )
}

const Assistant = () => {
  const logs = useStore(state => state.app.logs)

  const taskData = [
    ['Upgrade Gear', taskLib.upgradeGearIndefinitely],
    ['Daily Routine', taskLib.dailyRoutine],
    ['Open Prize Boxes', taskLib.pullOnPrizeBox],
    ['Repeat Gear Battle'],
    ['Exchange Gears'],
  ]

	return (
		<>
			<Box
        margin={1}
        borderColor='white'
        borderStyle='round'
        flexDirection='column'
      >
			  <Box
          paddingX='4%'
          paddingBottom={1}
        >
          <TitleBar title='PMEX Assistant'/>
        </Box>
        {taskData.map(([name, onEnter], index) => (
          <Task key={index} number={index + 1} name={name} onEnter={onEnter} />
        ))}
			</Box>
      <Box
        marginTop={1}
        marginX={2}
        flexDirection='column'
      >
        <LogWindow logs={logs} />
      </Box>
		</>
	)
}

module.exports = Assistant
