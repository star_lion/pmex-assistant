const R = require('ramda')
const nutjs = require('@nut-tree/nut-js')
const utils = require('nut-js-utils')

const useStore = require('./store/useStore')
const { executeEventSequence } = require('nut-js-utils')

const {mouse, sleep, randomPointIn} = nutjs

const store = useStore.getState()
const uiTargetFractionCorners = store.app.uiTargets.relative.fractionRegionCorners
const {
  back, shopTab, purchaseGems, gemsDailyTab, gemsDaily, gemsOk,
  syncScout, syncScoutLeftArrow, syncDailyScout, syncSkipAnimation
} = uiTargetFractionCorners

const tasks = {}

tasks.dailyRoutine = async () => {
  const n9Region = useStore.getState().app.devices.n9.region
  const targetCorners = [
    shopTab,
    purchaseGems,
    gemsDailyTab,
    gemsDaily,
    gemsOk,
    // ?gemsWeekly,
    // ?gemsOk,
    // ?gemsMonthly,
    // ?gemsOk,
    back,
    syncScout,
    syncScoutLeftArrow,
    syncDailyScout,
    syncSkipAnimation,
    back,
    back,
    back,
  ]
  const regions = targetCorners.map((corners) => utils.relativeFractionRegionCornersToAbsoluteRegion(corners, n9Region))
  const randomizedPoints = await Promise.all(regions.map(randomPointIn))
  store.app.log('dailyRoutine start')
  for (const point of randomizedPoints) {
    store.app.log(`${point.toString()}`)
    await sleep(3000)
    await mouse.setPosition(point)
    await mouse.leftClick()
  }
  store.app.log('dailyRoutine end')
}

tasks.pullOnPrizeBox = async () => {
  const n9Region = useStore.getState().app.devices.n9.region
  const targetCorners = [
    uiTargetFractionCorners.prizeBox.tenPull,
    uiTargetFractionCorners.prizeBox.yes
  ]
  const regions = targetCorners.map((corners) => utils.relativeFractionRegionCornersToAbsoluteRegion(corners, n9Region))
  store.app.log('prize box pull start')
  while (true) {
    //pull box
    const randomizedPoints = await Promise.all(regions.map(randomPointIn))
    await sleep(2250)
    await mouse.setPosition(randomizedPoints[0])
    await mouse.leftClick()
    await sleep(1000)
    await mouse.setPosition(randomizedPoints[1])
    await mouse.leftClick()
    await sleep(500)
    await mouse.leftClick()
    await sleep(500)
    await mouse.leftClick()
  }
  store.app.log('prize box pulling end')
}

tasks.upgradeGearIndefinitely = async () => {
  const state = useStore.getState()
  const n9Region = state.app.devices.n9.region
  const {
    upgrade, upgradeAgain, yes
  } = uiTargetFractionCorners.gear
  const targetCorners = [
    upgrade, yes, upgradeAgain
  ]

  while (true) {
    state.app.log('Upgrading gear')
    const regions = targetCorners.map((corners) => utils.relativeFractionRegionCornersToAbsoluteRegion(corners, n9Region))
    const randomizedPoints = await Promise.all(regions.map(randomPointIn))
    const events = randomizedPoints.map((point) => [point, true, 2500])
    await utils.executeEventSequence(events, state.app.logInPlace)
  }
}

module.exports = tasks
