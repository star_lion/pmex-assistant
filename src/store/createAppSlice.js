const nutjs = require('@nut-tree/nut-js')
const {produce} = require('immer')
const R = require('ramda')
const {exec} = require('child_process')

const {mouse, getWindows, Point, screen} = nutjs

const createAppSlice = (set, get) => ({
  app: {

    logs: [],

    devices: {
      n9: {
        title: 'SM-N960U1',
        serial: '299b2eedfb1c7ece',
        window: undefined,
        region: undefined
      },
    },

    uiTargets: {
      relative: {
        fractionRegionCorners: {
          back: [[0.035,0.938], [0.102,0.964]],
          shopTab: [[0.298,0.896], [0.411,0.945]],
          purchaseGems: [[0.141,0.350], [0.846,0.396]],
          gemsDailyTab: [[0.305,0.853], [0.475,0.884]],
          gemsDaily: [[0.091,0.202], [0.901,0.279]],
          gemsOk: [[0.448,0.602], [0.554,0.624]],
          syncScout: [[0.143,0.131], [0.822,0.176]],
          syncScoutLeftArrow: [[0.044,0.368], [0.077,0.424]],
          syncDailyScout: [[0.244,0.733], [0.747,0.780]],
          syncSkipAnimation: [[0.410,0.660], [0.420,0.668]],
          prizeBox: {
            tenPull: [[0.556,0.829], [0.895,0.882]],
            yes: [[0.578,0.720], [0.846,0.754]],
          },
          gear: {
            upgrade: [[0.360,0.929], [0.642,0.973]],
            upgradeAgain: [[0.344,0.773], [0.653,0.803]],
            yes: [[0.578,0.715], [0.870,0.754]],
            ok: []
          },
        }
      },
    },

    getDevices: async () => {
      const devices = get().app.devices
      const windows = await getWindows()
      const [windowTitles, windowRegions] = await Promise.all([
        Promise.all(windows.map((window) => window.title)),
        Promise.all(windows.map((window) => window.region))
      ])

      const n9Index = R.findIndex(R.equals(devices.n9.title), windowTitles)
      if (n9Index != -1) {
        screen.highlight(windowRegions[n9Index])
        set(produce(state => {
          state.app.devices.n9.window = windows[n9Index]
          state.app.devices.n9.region = windowRegions[n9Index]
        }))
      } else {
        set(produce(state => {
          state.app.devices.n9.window = undefined
          state.app.devices.n9.region = undefined
        }))
      }
    },

    pasteToN9: (text) => {
      const serial = get().app.devices.n9.serial
      exec(`adb -s ${serial} shell "input text ${text} && input keyevent 61 && input keyevent 66"`)
    },

    log: (text) => set(produce(state => {
      const now = new Date()
      const timestamp = `[${now.toLocaleTimeString('en-US')}]`
      state.app.logs.push(`${timestamp} ${text}`)
    })),

    logInPlace: (text) => set(produce(state => {
      const logsLength = get().app.logs.length
      const latestLogIndex = logsLength - 1
      if (logsLength == 0) {
        state.app.log(text)
      } else {
        state.app.logs[latestLogIndex] = state.app.logs[latestLogIndex] + text
      }
    })),

    beep: (pitch=400, durationMs=100) => {
      if (process.platform != 'win32') return
      exec(`powershell.exe [console]::beep(${pitch},${durationMs})`)
    },

    beepLow: (durationMs=100) => {
      get().app.beep(200, durationMs)
    },

    beepHigh: (durationMs=100) => {
      get().app.beep(600, durationMs)
    }


  }
})

module.exports = createAppSlice
