const create = require('zustand').default
const immer = require('immer')
const nutjs = require('@nut-tree/nut-js')
const R = require('ramda')

const {mouse, getWindows, Point} = nutjs
const {produce} = immer

const utils = require('nut-js-utils')
const createAppSlice = require('./createAppSlice.js')

const DEBUG = false

const useStore = create((set, get) => ({
  ...createAppSlice(set, get),
}))


const store = useStore.getState()

store.app.getDevices()
store.app.log('assistant loaded')

module.exports = useStore
