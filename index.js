#!/usr/bin/env node
'use strict';
const React = require('react');
const importJsx = require('import-jsx');
const {render} = require('ink');
const meow = require('meow');

const Assistant = importJsx('./src/Assistant');

const cli = meow(`
	Usage
		$ pmex-assistant

	Options
		--name  Your name

	Examples
		$ pmex-assistant --name=Jane
		Hello, Jane
`);

process.on('uncaughtException', function(error) {
  console.log('Caught exception: ' + error);
  process.exit()
});

render(React.createElement(Assistant, cli.flags));
